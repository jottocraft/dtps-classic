var dtps = {
  ver: 100,
  readableVer: "v1.0.0-c",
  trackSuffix: "",
  showLetters: false
};
dtps.changelog = function () {
  fluid.cards.close(".card.focus")
  fluid.cards(".card.changelog");
};
dtps.log = function (msg) {
  console.log("[DTPS] ", msg);
  if (typeof msg !== "object") { try { jQuery(".card.console .log").html(`<h5>[DTPS] ` + msg + `</h5>` + jQuery(".card.console .log").html()); } catch (e) { } }
}
dtps.firstrun = function () {
  jQuery("body").append(`<div id="TB_overlay" style="position: fixed;">&nbsp;</div><div id="TB_window" role="dialog" aria-modal="true" aria-labelledby="TB_title" style="width: 800px; height: 540px;margin: 0 calc(50% - 400px); top: calc(50% - 290px);"><div id="TB_closeAjaxWindow" class="tb_title_bar" role="heading"><a href="javascript:;" onclick="TB_remove();" id="TB_closeWindowButton" aria-hidden="true"><i class="icon-close"></i></a><div id="TB_title" class="tb_title">Project DTPS</div><div id="TB_ajaxContent" role="main" style="width: 770px; height: 434px;">
<h2>Welcome to Project DTPS</h2>
<h4>` + dtps.readableVer + `</h4>
<li>Project DTPS is meant to be simple, so many PowerSchool features will be left out</li>
<li>Project DTPS does not store any personal user data (such as grades). Data Project DTPS does use and needs to store (such as prefrences and version info for changelogs) is stored locally on your computer, never in any online database. Project DTPS uses Google Analytics to track how many people are using DTPS and will never log your ID or IP address with Google Analytics</li>
<li>Project DTPS only reads data from PowerSchool. Project DTPS will never edit, write, or delete data of any kind on your PowerSchool account</li>
<li>Project DTPS needs to be loaded with the bookmark script every time (unless using the chrome extension). You can always use PowerSchool as normal by reloading and not clicking the bookmark</li>
<li>Report bugs and send feedback by clicking the feedback button at the top right corner</li>
<li><b>Project DTPS may have bugs that cause it to display an inaccurate representation of your grades and assignments. Use Project DTPS at your own risk.</b></li>
</div><div id="TB_actionBar" style=""><span><input class="button button" onclick="ThickBox.close();" type="button" value="Cancel"><input class="button button" onclick="ThickBox.close(); localStorage.setItem('dtpsInstalled', 'true'); dtps.render();" type="button" value="Accept & Continue"></span>
`)
};
dtps.alert = function (text, sub) {
  if (text == undefined) var text = "";
  if (sub == undefined) var sub = "";
  jQuery("body").append(`<div id="TB_overlay" style="position: fixed;">&nbsp;</div><div id="TB_window" role="dialog" aria-modal="true" aria-labelledby="TB_title" style="width: 800px; height: 540px;margin: 0 calc(50% - 400px); top: calc(50% - 290px);"><div id="TB_closeAjaxWindow" class="tb_title_bar" role="heading"><div id="TB_title" class="tb_title">Project DTPS</div><div id="TB_ajaxContent" role="main" style="width: 770px; height: 434px;">
<h2>` + text + `</h2>
<p>` + sub + `</p>
</div>
`)
};
dtps.requests = {};
dtps.http = {};
dtps.webReq = function (req, url, callback, q) {
  if (dtps.requests[url] == undefined) {
    if (req == "psGET") {
      dtps.http[url] = new XMLHttpRequest();
      dtps.http[url].onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          if (callback) callback(this.responseText, q);
          dtps.requests[url] = this.responseText;
        }
      };
      dtps.http[url].open("GET", url, true);
      dtps.http[url].setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
      dtps.http[url].setRequestHeader("Accept-Language", "en-US,en;q=0.9")
      dtps.http[url].setRequestHeader("Upgrade-Insecure-Requests", "1")
      dtps.http[url].send();
    }
    if (req == "psPOST") {
      dtps.http[url] = new XMLHttpRequest();
      dtps.http[url].onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          if (callback) callback(this.responseText, q);
          dtps.requests[url] = this.responseText;
        }
      }
      dtps.http[url].open("POST", url, true);
      dtps.http[url].setRequestHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*")
      dtps.http[url].setRequestHeader("Accept-Language", "en-US,en;q=0.9")
      dtps.http[url].setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8")
      dtps.http[url].setRequestHeader("X-Prototype-Version", "1.7.1")
      dtps.http[url].setRequestHeader("X-Requested-With", "XMLHttpRequest")
      dtps.http[url].send("csrf_token=" + CSRFTOK);
    }
    if (req == "letPOST") {
      dtps.http[url] = new XMLHttpRequest();
      dtps.http[url].onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          if (callback) callback(this.responseText, q);
          dtps.requests[url] = this.responseText;
        }
      }
      dtps.http[url].open("POST", url, true);
      dtps.http[url].setRequestHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*")
      dtps.http[url].setRequestHeader("Accept-Language", "en-US,en;q=0.9")
      dtps.http[url].setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8")
      dtps.http[url].setRequestHeader("X-Prototype-Version", "1.7.1")
      dtps.http[url].setRequestHeader("X-Requested-With", "XMLHttpRequest")
      dtps.http[url].send(portalClassesAndUserQuery() + "&csrf_token=" + CSRFTOK);
    }
  } else {
    if (callback) callback(dtps.requests[url], q);
  }
}
dtps.init = function () {
  dtps.log("Starting DTPS " + dtps.readableVer + "...");
  sudoers = ["669", "672", "560"]
  if (sudoers.includes(ENV.current_user.id)) { jQuery("body").addClass("sudo"); dtps.log("Sudo mode enabled"); }
  og = ["560", "669", "672"]
  if (og.includes(ENV.current_user.id)) { jQuery("body").addClass("og"); }
  contributors = ["669"]
  if (contributors.includes(ENV.current_user.id)) { jQuery("body").addClass("contributor"); }
  if (ENV.current_user.id == "669") { jQuery("body").addClass("dev"); dtps.log("Dev mode enabled"); }
  dtps.shouldRender = false;
  dtps.first = false;
  dtps.showChangelog = false;
  dtps.updateScript = false;
  dtps.user = ENV.current_user;
  dtps.classColors = [];
  if (false && (window.location.host !== "dtechhs.learning.powerschool.com")) {
    dtps.shouldRender = false;
    dtps.alert("Unsupported school", "Project DTPS only works at Design Tech High School");
  } else {
    if (false && (Number(window.localStorage.dtps) < dtps.ver)) {
      dtps.showChangelog = true;
      //Load fluid JS modules early for changelogs
      $ = jQuery;
      jQuery.getScript('https://classic.dtps.jottocraft.com/fluid.js');
      dtps.shouldRender = true;
      dtps.alert("Loading...", "Updating to DTPS " + dtps.readableVer);
    } else {
      if (false) {
        dtps.shouldRender = false;
        dtps.alert("Unsupported Account", "Project DTPS only works on student accounts");
      } else {
        if (false && (window.dtpsLoader < 2)) {
          dtps.updateScript = true;
          dtps.shouldRender = true;
          dtps.alert("Loading...", "Make sure to update your bookmark script");
        } else {
          dtps.shouldRender = true;
          dtps.alert("Loading...");

        }
      }
    }

    if (window.localStorage.dtpsInstalled !== "true") {
      dtps.shouldRender = false;
      dtps.first = true;
    }
  }
  localStorage.setItem('dtps', dtps.ver);
  dtps.classesReady = 0;
  jQuery.getScript('https://classic.dtps.jottocraft.com/fluid.js', function () {
    jQuery.getJSON("/api/v1/users/self/colors", function(colors) {
      jQuery.getJSON("/api/v1/users/self/courses?include[]=total_scores&per_page=100&enrollment_state=active", function (data) {
        dtps.rawData = data;
        dtps.classes = [];
        for (var i = 0; i < data.length; i++) {
          dtps.classes.push({
            name: data[i].name,
            subject: data[i].name.split(" - ")[0],
            abbrv: data[i].name.substring(0, 2).toUpperCase(),
            grade: data[i].enrollments[0]?.computed_current_score || "--",
            letter: data[i].enrollments[0]?.computed_current_grade || "--",
            loc: "/courses/" + data[i].id,
            col: colors.custom_colors["course_" + data[i].id],
            id: data[i].id,
            num: i
          });
          dtps.classStream(i, true);
        }
        dtps.log("Grades loaded: ", dtps.classes)
        if (dtps.shouldRender) dtps.render();
        if (dtps.first) dtps.firstrun();
      });
    });
  });
}
dtps.checkReady = function (num) {
  //dtps.log(num + " reporting as READY total of " + dtps.classesReady);
  if ((dtps.selectedClass == "stream") && (dtps.classesReady == dtps.classes.length)) {
    dtps.log("All classes ready, loading master stream");
    dtps.masterStream(true);
  } else {
    if ((dtps.selectedClass == "stream") && (dtps.classesReady < dtps.classes.length)) {
      dtps.masterStream();
    }
  }
}
dtps.loadPages = function (num) {
  if ((dtps.selectedClass == num) && (dtps.selectedContent == "pages")) {
    jQuery(".sidebar").html(`
<div class="classDivider"></div>
<div class="spinner">
  <div class="bounce1"></div>
  <div class="bounce2"></div>
  <div class="bounce3"></div>
</div>
`);
    jQuery(".classContent").html("");
  }
  jQuery.getJSON("/api/v1/courses/" + dtps.classes[num].id + "/pages?per_page=100", function (data) {
    dtps.log("GOT DATA", data)
    dtps.rawData = data;
    dtps.classes[num].pages = [];
    dtps.classes[num].pagelist = [];
    if (!data.length) data = [];
    for (var i = 0; i < data.length; i++) {
      dtps.classes[num].pages.push({
        id: data[i].page_id,
        title: data[i].title,
        content: "",
        num: i
      });
      dtps.classes[num].pagelist.push(`
      <div onclick="dtps.selectedPage = '` + data[i].page_id + `'" class="class">
      <div class="name">` + data[i].title + `</div>
      <div class="grade"><i class="material-icons">notes</i></div>
      </div>
      `);
    }
    if ((dtps.selectedClass == num) && (dtps.selectedContent == "pages")) {
      jQuery(".sidebar").html(`<div onclick="dtps.classStream(dtps.selectedClass);" class="class back">
      <div class="name">Classes</div>
      <div class="grade"><i class="material-icons">keyboard_arrow_left</i></div>
      </div>
      <div class="classDivider"></div>
    ` + dtps.classes[num].pagelist.join(""))
    }

    $(".class").click(function (event) {
      if (!$(this).hasClass("back")) {
        $(this).siblings().removeClass("active")
        $(this).addClass("active")
        dtps.getPage(dtps.classes[dtps.selectedClass].loc);
      }
    });
  }).fail(() => {
    alert("This class doesn't support pages");
  });
}
dtps.classStream = function (num, renderOv) {
  dtps.log("rendering stream for " + num)
  dtps.showClasses();
  if ((dtps.selectedClass == num) && (dtps.selectedContent == "stream")) {
    if (!renderOv) {
      jQuery(".classContent").html(`
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>
  `);
    }
  }
  jQuery.getJSON("https://dtechhs.instructure.com/api/v1/users/self/courses/" + dtps.classes[num].id + "/assignments?per_page=100&include[]=submission", function (data) {
    dtps.classes[num].stream = [];
    dtps.classes[num].streamlist = [];
    dtps.classes[num].streamitems = [];
    for (var i = 0; i < data.length; i++) {
      var assignment = data[i]
      var due = "<h5>Due " + new Date(assignment.due_at).toLocaleString("en", { weekday: 'short', month: 'short', day: 'numeric', hour: "numeric", minute: "numeric" }) + "</h5>"
      if (!assignment.due_at) var due = "";
      dtps.classes[num].stream.push({
        id: assignment.id,
        title: assignment.name,
        due: assignment.due_at,
        col: dtps.classes[num].col,
        grade: assignment.submission?.score ? assignment.submission?.score + "/" + assignment.points_possible : "",
        letter: "A"
      });
      dtps.classes[num].streamitems.push(assignment.id);
      dtps.classes[num].streamlist.push(`
          <div class="card assignment">
          <div class="spinner points">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
          </div>
          <h4>` + assignment.name + `</h4>
          ` + due + `
          </div>
      `);
    }
    if (!renderOv) jQuery(".classContent").html(dtps.classes[num].streamlist.join(""));
    dtps.classes[num].streamlist = [];
    if ((dtps.selectedClass == num) && (dtps.selectedContent == "stream")) { if (!renderOv) { jQuery(".classContent").html(dtps.renderStream(dtps.classes[num].stream, dtps.classes[num].col)); } }
    dtps.classesReady++;
    dtps.checkReady(num);
  });
}
dtps.renderStream = function (stream, col) {
  var streamlist = [];
  if (col == undefined) var col = "";
  for (var i = 0; i < stream.length; i++) {
    var due = "Due " + new Date(stream[i].due).toLocaleString("en", { weekday: 'short', month: 'short', day: 'numeric', hour: "numeric", minute: "numeric" });
    if (!stream[i].due) var due = "";
    if ((stream[i].grade !== "-") && (stream[i].grade)) {
      if ("ABC".includes(stream[i].letter.substring(0, 1))) {
        var earnedTmp = stream[i].grade.split("/")[0];
      } else {
        var earnedTmp = stream[i].letter;
      }
      if (dtps.showLetters) earnedTmp = stream[i].letter;
      streamlist.push(`
        <div style="--classColor: ` + stream[i].col + `;" class="card graded assignment">
        <div class="points">
        <div class="earned">` + earnedTmp + `</div>
        <div class="total">/` + stream[i].grade.split("/")[1] + `</div>
        </div>
        <h4>` + stream[i].title + `</h4>
      	<h5>` + due + `</h5>
        </div>
      `);
    } else {
      streamlist.push(`
        <div style="--classColor: ` + stream[i].col + `;" class="card assignment">
        <h4>` + stream[i].title + `</h4>
	       <h5>` + due + `</h5>
         </div>
       `);
    }
  }
  return streamlist.join("");
}
dtps.masterStream = function (doneLoading) {
  dtps.showClasses();
  if (dtps.selectedClass == "stream") {
    jQuery(".classContent").html(`
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>
  `);
  }
  var buffer = [];
  for (var i = 0; i < dtps.classes.length; i++) {
    if (dtps.classes[i].stream) {
      dtps.log("BUILDING: " + i)
      buffer = buffer.concat(dtps.classes[i].stream)
    }
  }
  dtps.log(buffer)
  var loadingDom = "";
  if (!doneLoading) {
    loadingDom = `<div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>`;
  }
  if (dtps.selectedClass == "stream") {
    jQuery(".classContent").html(loadingDom + dtps.renderStream(buffer.sort(function (a, b) {
      var keyA = new Date(a.due),
        keyB = new Date(b.due);
      // Compare the 2 dates
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    })));
  }
  $(".card.assignment").addClass("color");
}
dtps.getPage = function (loc, id) {
  if (id == undefined) var id = dtps.selectedPage;
  if ((dtps.classes[dtps.selectedClass].loc == loc) && (dtps.selectedContent == "pages")) {
    jQuery(".classContent").html(`
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>
  `);
  }
  var spinnerTmp = true;
  jQuery.getJSON("/api/v1/courses/" + dtps.classes[dtps.selectedClass].id + "/pages/" + id, function (data) {
    if ((dtps.classes[dtps.selectedClass].loc == loc) && (dtps.selectedContent == "pages")) { if (spinnerTmp) { jQuery(".classContent").html(""); spinnerTmp = false; } }
    if ((dtps.classes[dtps.selectedClass].loc == loc) && (dtps.selectedContent == "pages")) {
      jQuery(".classContent").html(jQuery(".classContent").html() + `
      <div class="card">
     <h4>` + data.title + `</h4>
      ` + data.body + `
      </div>
    `);
    }
  });
}
dtps.announcements = function () {
  if (dtps.selectedClass == "announcements") {
    jQuery(".classContent").html(`
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>
  `);
  }

  function contextCodesObj(arr) {
    if (arr.length) {
      var param = "?";
      arr.forEach(val => {
        if (param !== "?") param += "&";
        param += "context_codes[]=course_" + val;
      });
      return param;
    } else {
      return "";
    }
  }

  jQuery.getJSON("/api/v1/announcements" + contextCodesObj(dtps.classes.map(c => c.id)) + "&per_page=100", function (data) {
    dtps.raw = data;
    var announcements = [];
    for (var i = 0; i < data.length; i++) {
      announcements.push(`<div class="card">
` + data[i].message + `
</div>
`);
    }
    if (dtps.selectedClass == "announcements") {
      jQuery(".classContent").html(announcements.join(""));
    }
  });
};
dtps.showClasses = function () {
  var streamClass = "active"
  if (dtps.selectedClass !== "stream") var streamClass = "";
  dtps.classlist = [];
  for (var i = 0; i < dtps.classes.length; i++) {
    dtps.classlist.push(`
      <div style="--classColor: ` + dtps.classes[i].col + `;" onclick="dtps.selectedClass = ` + i + `" class="class ` + i + `">
      <div class="name">` + dtps.classes[i].subject + `</div>
      <div class="grade"><span class="letter">` + dtps.classes[i].letter + `</span><span class="points">` + dtps.classes[i].grade + `%</span></div>
      </div>
    `);
  }
  jQuery(".sidebar").html(`<div onclick="dtps.selectedClass = 'stream';" class="class ` + streamClass + `">
    <div class="name">Stream</div>
    <div class="grade"><i class="material-icons">view_stream</i></div>
    </div>
   <div onclick="dtps.selectedClass = 'announcements';" class="class">
    <div class="name">Announcements</div>
    <div class="grade"><i class="material-icons">experiment</i></div>
    </div>
    <div class="classDivider"></div>
  ` + dtps.classlist.join(""));
  if (dtps.classes[dtps.selectedClass]) $(".class." + dtps.selectedClass).addClass("active");
  if ($(".btn.pages").hasClass("active")) { $(".btn.pages").removeClass("active"); $(".btn.stream").addClass("active"); dtps.classStream(dtps.selectedClass); dtps.selectedContent = "stream"; }
  $(".class").click(function (event) {
    if (dtps.classes[dtps.selectedClass])  { $(".background").css("background", dtps.classes[dtps.selectedClass].col) } else { $(".background").css("background", "var(--grad)") }
    $(this).siblings().removeClass("active")
    $(this).addClass("active")
    $(".header h1").html($(this).children(".name").text())
    if (!dtps.classes[dtps.selectedClass]) {
      $(".header .btns").hide();
    } else {
      $(".header .btns").show();
    }
    if ((dtps.selectedContent == "stream") && (dtps.classes[dtps.selectedClass])) dtps.classStream(dtps.selectedClass)
    if ((dtps.selectedContent == "grades") && (dtps.classes[dtps.selectedClass])) dtps.gradebook(dtps.selectedClass)
    if (dtps.selectedClass == "stream") dtps.masterStream(true);
    if (dtps.selectedClass == "announcements") dtps.announcements();
    if (dtps.classes[dtps.selectedClass]) { if (dtps.classes[dtps.selectedClass].weights) { if (dtps.classes[dtps.selectedClass].weights.length) { $(".btns .btn.grades").show(); } else { $(".btns .btn.grades").hide(); } } else { $(".btns .btn.grades").hide(); } }
  });
}
dtps.render = function () {
  document.title = "Project DTPS" + dtps.trackSuffix;
  $ = jQuery;
  dtps.selectedClass = "stream";
  dtps.selectedContent = "stream";
  if (window.localStorage.fluidIsDark == "true") { var dark = " active" } else { var dark = "" }
  jQuery("body").html(`
    <div class="sidebar">
    </div>
    <div style="background: var(--grad);" class="background"></div>
<div class="header">
    <h1 id="headText">Stream</h1>
    <div style="display: none;" class="btns row">
    <button onclick="dtps.selectedContent = 'stream'; dtps.classStream(dtps.selectedClass);" class="btn active stream">
    <i class="material-icons">view_stream</i>
    Stream
    </button>
    <button onclick="dtps.selectedContent = 'pages'; dtps.loadPages(dtps.selectedClass);" class="btn pages">
    <i class="material-icons">list</i>
    Pages
    </button>
    <button onclick="dtps.selectedContent = 'grades'; dtps.gradebook(dtps.selectedClass);" class="btn grades">
    <i class="material-icons">book</i>
    Gradebook
    </button>
    </div>
    <div class="classContent">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
    </div>
    </div>
    <div style="width: calc(80%);border-radius: 30px;" class="card focus close abt">
<i onclick="fluid.cards.close('.card.abt')" class="material-icons close">close</i>
    <h3>About</h3>
    <h5>Project DTPS ` + dtps.readableVer + `</h5>
    Logged in as ` + dtps.user.display_name + ` (` + dtps.user.id + `)<div style="display:inline-block;" class="beta badge notice sudo">tester&nbsp;<i style="vertical-align: middle;" class="material-icons sudo">experiment</i></div><div style="display:inline-block;" class="beta badge notice dev">developer&nbsp;<i style="vertical-align: middle;" class="material-icons dev">code</i></div><div style="display:inline-block;" class="beta badge notice og">OG&nbsp;<i style="vertical-align: middle;" class="material-icons og">star</i></div><div style="display:inline-block;" class="beta badge notice contributor">contributor&nbsp;<i style="vertical-align: middle;" class="material-icons contributor">group</i></div>
    <p>Made by <a href="https://github.com/jottocraft">jottocraft</a></p>
    <br />
    <div onclick="fluid.dark();" class="switch` + dark + `"><span class="head"></span></div>
    <div class="label"><i class="material-icons">brightness_3</i> Use dark theme</div>
<br /><br />
<div onclick="if (dtps.showLetters) {dtps.showLetters = false;} else {dtps.showLetters = true;}" class="switch sudo"><span class="head"></span></div>
    <div class="label sudo"><i class="material-icons">experiment</i> Show letter grades instead of points earned</div>
    <br /><br />
<div onclick="jQuery('body').toggleClass('hidegrades')" class="switch sudo"><span class="head"></span></div>
    <div class="label sudo"><i class="material-icons">experiment</i> Hide grades</div>
    <br /><br />
    <button onclick="dtps.changelog();" style="display:none;" class="btn changelog"><i class="material-icons">update</i>Changelog</button>
    </div>
    <div class="items">
    <h4>` + dtps.user.display_name + `</h4>
    <i onclick="fluid.cards('.abt')" class="material-icons">info_outline</i>
    <i onclick="fluid.cards('.console')" class="material-icons dev">code</i>
    <i onclick="window.open('https://github.com/jottocraft/dtps/issues/new/choose')" class="material-icons">feedback</i>
    </div>
<div  style="width: calc(80%);border-radius: 30px;" class="card focus changelog close">
<i onclick="fluid.cards.close('.card.changelog')" class="material-icons close">close</i>
<h3>What's new in Project DTPS</h3>
<h5>There was an error loading the changelog. Try again later.</h5>
</div>
<div  style="width: calc(80%);border-radius: 30px;" class="card focus console close">
<i onclick="fluid.cards.close('.card.console')" class="material-icons close">close</i>
<h3>dtps.log</h3>
<span class="log">
</span>
</div>
<div  style="width: calc(80%);border-radius: 30px;" class="card focus script close">
<i onclick="fluid.cards.close('.card.script')" class="material-icons close">close</i>
<h3>Update your DTPS bookmark</h3>
<p>It looks like you're using an outdated version of the Project DTPS bookmark. While this may work for now, you may run into some issues in the future. Right click the bookmark, select "Edit", and replace the URL with the <a href="https://classic.dtps.jottocraft.com/bookmark.txt">latest script</a>.</p>
</div>
  `);
  jQuery.getScript("https://cdn.rawgit.com/showdownjs/showdown/1.8.6/dist/showdown.min.js", function () {
    markdown = new showdown.Converter();
    jQuery.getJSON("https://api.github.com/repos/jottocraft/dtps/releases", function (data) {
      jQuery(".card.changelog").html(`<i onclick="fluid.cards.close('.card.changelog')" class="material-icons close">close</i>` + markdown.makeHtml(data[0].body));
      if (dtps.showChangelog) dtps.changelog();
      if (dtps.updateScript) { fluid.cards.close(".card.focus"); fluid.cards(".card.script"); }
      $(".btn.changelog").show();
    });
  });

  dtps.showClasses();
  $("link").remove();
  jQuery("<link/>", {
    rel: "shortcut icon",
    type: "image/png",
    href: "https://classic.dtps.jottocraft.com/favicon.png"
  }).appendTo("head");
  jQuery("<link/>", {
    rel: "stylesheet",
    type: "text/css",
    href: "https://classic.dtps.jottocraft.com/fluid.css"
  }).appendTo("head");
  jQuery("<link/>", {
    rel: "stylesheet",
    type: "text/css",
    href: "https://classic.dtps.jottocraft.com/dtps.css"
  }).appendTo("head");
  jQuery("<link/>", {
    rel: "stylesheet",
    type: "text/css",
    href: "https://fonts.googleapis.com/icon?family=Material+Icons+Extended"
  }).appendTo("head");
  fluid.init();
}
dtps.init();

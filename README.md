## Power+ Classic Edition
This repository contains a remastered version of the first version of Power+ from 2018, with support for Canvas LMS. Everything is as close to the original version as possible, however, there are some missing features (such as the gradebook). Some UI changes were also made in order to provide basic feature support for Canvas classes (such as the removal of class gradients). This project is mainly for fun and shouldn't be used for anything important since there are so many bugs and missing features.

Power+ Classic edition can be loaded by clicking [here](https://dtechhs.instructure.com/power+?classicEdition=true) or by spam-clicking the "Power+" text in the about tab of settings on a modern version of Power+.

**The original README.md file is below:**

---

[Install using bookmark script](https://dtps.js.org/bookmark.txt)

or

[Install Chrome Extension (auto load)](https://chrome.google.com/webstore/detail/project-dtps/pakgdifknldaiglefmpkkgfjndemfapo)

## Moving to dtps.js.org
As Project DTPS becomes finalized and ready for initial release, DTPS will be moving to dtps.js.org. Update to the [newer script](https://dtps.js.org/bookmark.txt) if you haven't already (this only applies to people that have used Project DTPS before 11/3).

## Project DTPS News

DTPS v1.0.0 stable target release date is 11/5 (PM Release) (testing (GM / v0.9.x) 11/2 or 11/3 - 11/5)
Project DTPS will open to contributions on 11/8

**sudoer (experimental) features:**
* show letter grades on all assignments (v1.0.0+)
* PE P/DV grading (v1.0.0+)
* hide grades from class list (v1.0.0+)

### Pre-Release To-Do List
Just because something is checked off on this list does not mean it is fully released. Checked off items are items that are present (even if only for devs) and basically finished. Checked off items do not mean that the corresponding release has been released.

*Beta 1 - 1.2*

* ~~Dark mode UI toggle~~
* ~~Feedback and user name somewhere~~
* ~~About menu~~
* ~~Class colors~~
* ~~Master/all classes stream~~
* ~~Speed improvements~~

*Beta 2 - 2.2 11/1*

* ~~Fix animations (gradient)~~
* ~~Multi-block pages~~
* ~~block titles~~
* ~~Pages speed improvements~~
* ~~show assignment weight category in stream~~

*Beta 3 11/1 (early release)*

* ~~dtps.masterStream speed improvements and fix stream bug (almost ready)~~
* ~~Pull Changelogs from GitHub and use Fluid for changelogs~~
* ~~Gradebook tab~~
* ~~Figure out why PowerSchool keeps clearing DTPS cookies and fix it :( (maybe use local storage?)~~

*v0.9.x Late 11/2 or Early 11/3 - Late 11/4 or Early 11/5*
* GMs (Release candidates)
* each .x revision contains bugfixes and polishes. no new features in any GM release.
* each v0.9.x version is basically what the release will look like (versions ready to be released). there should only be like 2 of these (1 is ideal, but it probably won't happen because of how the gradebook is right now)

### Planned Features
* About page improvements
* Google Classroom integration (in classroom tab)
* Google Classroom integration (associate GC assignemnts with PS assignments in stream)
* P/DV grading for PE (in development for sudoers)
* Show letter grades on assignments (in development for sudoers)
* hide grades from class list (live for sudoers)
* Grade trend (with localstorage)
* ability to simulate a different grade on an assignment and have dtps calculate what your grade in the class would be if you had that grade (this and grade trend part of advanced gradebook update. low priority)

### Planned Features (from submitted feedback)
* n/a
